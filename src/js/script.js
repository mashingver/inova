(function ($) {
    $(document).ready(function () {

        var JSAPP = JSAPP || {};
        var body = $('body');

        JSAPP.animationInViewport = function () {
            $('.animated').whenInViewport(function ($element) {
                $element.addClass('inViewport');
            }, {
                threshold: -100, // difference in pixels from user scroll position
                setupOnce: true  // setup only once per dom element
            });
        };

        JSAPP.menu = function () {
            $('#menu-trigger').click(function () {
                body.toggleClass('menu-opened');
                return false;
            });
            $('#menu a').click(function () {
                body.removeClass('menu-opened');
            });
        };

        JSAPP.scroll = function() {
            var offsetTop = ($(window).width() >= 768) ? 50 : 30;
            $(window).scroll(function() {
                if(body.scrollTop() >= offsetTop) {
                    body.addClass('page-scrolled');
                } else {
                    body.removeClass('page-scrolled');
                }
            });
        };

        JSAPP.scrollMainMenu = function () {
            $('.menu-wr a').not('.not-anchor').click(function (e) {
                var anchor = $(this);
                body.removeClass("menu-opened");

                $('html, body').stop().animate({
                    scrollTop: $(anchor.attr('href')).offset().top - 58
                }, 1000);

                e.preventDefault();
            });
            return false;
        };

        //JSAPP.vhFix = function() {
        //    if ($(window).width() >= 768) {
        //        var wh = $(window).height();
        //        console.log(wh % 2);
        //        if ( wh % 2 !== 0) {
        //            $('#header').css({
        //                height: (wh+1)+'px'
        //            });
        //        } else {
        //            $('#header').css({
        //                height: '100vh'
        //            });
        //        }
        //    } else {
        //        $('#header').css({
        //            height: 'auto'
        //        });
        //    }
        //};

        JSAPP.menu();
        JSAPP.scrollMainMenu();
        JSAPP.animationInViewport();
        JSAPP.scroll();
        //JSAPP.vhFix();
        $(window).resize(function() {
            JSAPP.scroll();
            //JSAPP.vhFix();
        });

    });
})(jQuery);